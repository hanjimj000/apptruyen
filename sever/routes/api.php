<?php

use App\Http\Controllers\GenreController;
use App\Http\Controllers\StoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::get('/story', [StoryController::class, 'index']);
Route::get('story/{slug}', [StoryController::class, 'show']);
Route::post('/story', [StoryController::class, 'index']);
Route::post('/story/create', [StoryController::class, 'store']);

Route::get('/genre', [GenreController::class, 'index']);
Route::get('/filter-genre/{slug}', [GenreController::class, 'filter']);


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
