<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    use HasFactory;
    protected $table = 'story';

    public function genre()
    {
        return $this->belongsTo(Story::class);
    }

    public function episode()
    {
        return $this->hasMany(Episode::class);
    }
}
