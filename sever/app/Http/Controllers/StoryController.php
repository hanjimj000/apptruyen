<?php

namespace App\Http\Controllers;

use App\Models\Story;
use Illuminate\Http\Request;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $list =  Story::all();
    //   return $list;
        return response()->json($list,200);

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show( Request $request, $slug)
    {
        //

        $chapter = $request->query('chuong');


         $find = Story::with('episode')->where('slug',$slug)->first();
         
            return response()->json($find, 201);

        // if ($find !== null) {
        //     return response()->json($find, 201);
        // } else {
        //     return response()->json(['error' => 'Story not found'], 404);
        // }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
